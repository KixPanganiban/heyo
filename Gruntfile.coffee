module.exports = (grunt) ->
	grunt.initConfig
		cjsx:
			app:
				expand: true
				cwd: 'heyo/static/src'
				src: ['**/*.cjsx']
				dest: 'heyo/static/build/'
				ext: '.js'
				options:
					bare: true
		browserify:
			app:
				src: 'heyo/static/build/Main.js'
				dest: 'heyo/static/heyo.js'
				options:
					alias:
						'react': './heyo/static/vendor/react/react.js'
						'react-router': './heyo/static/vendor/react-router/build/umd/ReactRouter.min.js'
						'jquery': './heyo/static/vendor/jquery/dist/jquery.min.js'
						'lodash': './heyo/static/vendor/lodash/lodash.min.js'
						'js-cookie': './heyo/static/vendor/js-cookie/src/js.cookie.js'
		uglify:
			app:
				files:
					'heyo/static/heyo.min.js': 'heyo/static/heyo.js'
		watch:
			app:
				files: ['heyo/static/src/**/*']
				tasks: ['cjsx', 'browserify']

	grunt.loadNpmTasks 'grunt-browserify'
	grunt.loadNpmTasks 'grunt-contrib-watch'
	grunt.loadNpmTasks 'grunt-contrib-uglify'
	grunt.loadNpmTasks 'grunt-coffee-react'
	grunt.registerTask 'default', ['cjsx', 'browserify', 'uglify']