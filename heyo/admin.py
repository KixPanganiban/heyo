from django.contrib import admin
from .models import Profile, Testimonial, Photo, FacebookAuth

admin.site.register(Profile)
admin.site.register(Testimonial)
admin.site.register(Photo)
admin.site.register(FacebookAuth)
