var $, Cookies, ErrorMessage, InputGroup, LoginHandler, React, Router;

React = require('react');

Router = require('react-router');

$ = require('jquery');

Cookies = require('js-cookie');

InputGroup = require('./components/InputGroup.js');

ErrorMessage = require('./components/ErrorMessage.js');

LoginHandler = React.createClass({
  mixins: [Router.Navigation],
  getInitialState: function() {
    return {
      showError: false,
      errorMessage: "Hey"
    };
  },
  componentDidMount: function() {
    window.fbAsyncInit = function() {
      return FB.init({
        appId: '1630966230450897',
        cookie: true,
        xfbml: true,
        version: 'v2.1'
      });
    };
    return (function(d, s, id) {
      var fjs, js;
      js = void 0;
      fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {
        return;
      }
      js = d.createElement('script');
      js.id = id;
      js.src = '//connect.facebook.net/en_US/sdk.js';
      fjs.parentNode.insertBefore(js, fjs);
    })(document, 'body', 'facebook-jssdk');
  },
  checkLoginState: function() {
    var that;
    that = this;
    return FB.getLoginStatus((function(response) {
      if (response.status === "connected") {
        return FB.api('/me', (function(response) {
          return $.ajax({
            url: '/api/facebooklogin/',
            type: 'POST',
            data: {
              id: response.id
            },
            beforeSend: function(xhr) {
              return xhr.setRequestHeader("X-CSRFToken", Cookies.get("csrftoken"));
            },
            success: function(profileId) {
              return that.transitionTo('/profile/' + profileId);
            },
            error: function(xhr, status, err) {
              var errMsg;
              errMsg = err.toString();
              that.handleError(errMsg);
              return console.log(status, errMsg);
            }
          });
        }).bind(this));
      }
    }).bind(this));
  },
  handleFBLogin: function() {
    return FB.login(this.checkLoginState());
  },
  render: function() {
    return React.createElement("div", null, React.createElement("div", {
      "className": "row"
    }, React.createElement("h5", {
      "className": "header col s12 light"
    }, "Welcome back!")), React.createElement("div", {
      "className": "row"
    }, React.createElement("p", {
      "className": "header col light"
    }, "Enter your login details, or login with Facebook.")), React.createElement(ErrorMessage, {
      "show": this.state.showError,
      "message": this.state.errorMessage
    }), React.createElement("div", {
      "className": "row"
    }, React.createElement("form", {
      "className": "col s12",
      "ref": "loginForm",
      "action": "/api/login/",
      "method": "POST"
    }, React.createElement("div", {
      "className": "row"
    }, React.createElement(InputGroup, {
      "placeholder": "francis@mymail.com",
      "formName": "email",
      "type": "email",
      "className": "validate",
      "label": "Email Address"
    }), React.createElement(InputGroup, {
      "placeholder": "password",
      "formName": "password",
      "type": "password",
      "className": "validate",
      "label": "Password"
    })), React.createElement("div", {
      "className": "row"
    }, React.createElement("button", {
      "className": "waves-effect waves-light btn red",
      "onClick": this.handleSubmit
    }, "Login"), "\u00a0", React.createElement("button", {
      "className": "waves-effect waves-light btn blue darken-3",
      "onClick": this.handleFBLogin
    }, "Login with Facebook")))));
  },
  handleSubmit: function() {
    var loginForm, that;
    that = this;
    loginForm = $(React.findDOMNode(this.refs.loginForm));
    console.log(loginForm.attr('action'));
    return loginForm.submit(function(e) {
      var formData, obj, _i, _len, _ref;
      formData = {};
      _ref = loginForm.serializeArray();
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        obj = _ref[_i];
        formData[obj.name] = obj.value;
      }
      $.ajax({
        url: loginForm.attr('action'),
        type: 'POST',
        data: formData,
        beforeSend: function(xhr) {
          return xhr.setRequestHeader("X-CSRFToken", Cookies.get("csrftoken"));
        },
        success: function(profileId) {
          return that.transitionTo('/profile/' + profileId);
        },
        error: function(xhr, status, err) {
          var errMsg;
          errMsg = err.toString();
          that.handleError(errMsg);
          return console.log(status, errMsg);
        }
      });
      e.preventDefault();
      return e.stopImmediatePropagation();
    });
  },
  handleError: function(errMsg) {
    console.log("Handling error:", errMsg);
    if (errMsg === 'auth_fail') {
      return this.setState({
        showError: true,
        errorMessage: "Oops! Invalid email address and/or password. Please re-check your details and try again."
      });
    } else if (errMsg === 'no_fb_user') {
      return this.setState({
        showError: true,
        errorMessage: "Oops! That Facebook user isn't registered in Heyo! yet."
      });
    }
  }
});

module.exports = LoginHandler;
