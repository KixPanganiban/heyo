var $, Cookies, GetCurrentUserMixin, NotFoundView, PhotosView, ProfileBodyView, ProfileHandler, React, TestimonialView;

React = require('react');

$ = require('jquery');

Cookies = require('js-cookie');

GetCurrentUserMixin = require('./mixins/GetCurrentUserMixin.js');

NotFoundView = require('./components/NotFoundView.js');

TestimonialView = require('./components/TestimonialView.js');

PhotosView = require('./components/PhotosView.js');

ProfileBodyView = React.createClass({
  render: function() {
    return React.createElement("div", null, React.createElement("div", {
      "className": "row"
    }, React.createElement("div", {
      "className": "col s12"
    }, React.createElement("ul", {
      "className": "collection with-header"
    }, React.createElement("li", {
      "className": "collection-header"
    }, React.createElement("div", {
      "className": "row"
    }, React.createElement("div", {
      "className": "col s3"
    }, React.createElement("img", {
      "src": this.props.viewProfileImage,
      "className": "responsive-img circle"
    })), React.createElement("div", {
      "className": "col s9"
    }, React.createElement("h3", {
      "className": "red-text light"
    }, this.props.viewProfileData.name), React.createElement("p", {
      "className": "light"
    }, this.props.viewProfileData.bio)))), React.createElement("li", {
      "className": "collection-item"
    }, React.createElement("i", {
      "className": "mdi-maps-pin-drop"
    }, "\u00a0", this.props.viewProfileData.location)), React.createElement("li", {
      "className": "collection-item"
    }, React.createElement("i", {
      "className": "mdi-action-wallet-travel"
    }, "\u00a0", this.props.viewProfileData.work))))), React.createElement(PhotosView, {
      "profileId": this.props.viewProfileId
    }), React.createElement(TestimonialView, {
      "target_profile": this.props.viewProfileId,
      "viewProfileId": this.props.viewProfileId
    }));
  }
});

ProfileHandler = React.createClass({
  getInitialState: function() {
    return {
      viewProfileData: {},
      viewProfileImage: "",
      viewProfileId: this.props.params.profileId,
      status: "not_loaded"
    };
  },
  componentDidMount: function() {
    var that;
    that = this;
    return $.ajax({
      url: '/api/profile/',
      dataType: 'json',
      type: 'GET',
      data: {
        id: this.props.params.profileId
      },
      success: (function(viewProfileData) {
        return this.setState({
          viewProfileData: viewProfileData,
          status: "loaded",
          viewProfileImage: viewProfileData.profile_photo
        });
      }).bind(this),
      error: (function(xhr, status, err) {
        this.setState({
          status: "not_found"
        });
        return console.log(status, err.toString());
      }).bind(this)
    });
  },
  renderByStatus: function() {
    if (this.state.status === "loaded") {
      return React.createElement(ProfileBodyView, {
        "viewProfileId": this.state.viewProfileId,
        "viewProfileData": this.state.viewProfileData,
        "viewProfileImage": this.state.viewProfileImage
      });
    } else if (this.state.status === "not_found") {
      return React.createElement(NotFoundView, null);
    }
  },
  render: function() {
    return React.createElement("div", null, this.renderByStatus());
  }
});

module.exports = ProfileHandler;
