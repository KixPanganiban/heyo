var GetCurrentUserMixin;

GetCurrentUserMixin = {
  getCurrentUser: function() {
    return $.ajax({
      url: '/api/getcurrentuser/',
      type: 'GET',
      success: (function(profileId) {
        return this.setState({
          profileId: profileId
        });
      }).bind(this),
      error: (function(xhr, status, err) {
        this.setState({
          profileId: false
        });
        return console.log(status, err.toString());
      }).bind(this)
    });
  }
};

module.exports = GetCurrentUserMixin;
