var GetCurrentUserMixin, React, Router, WelcomeHandler;

React = require("react");

Router = require("react-router");

GetCurrentUserMixin = require('./mixins/GetCurrentUserMixin.js');

WelcomeHandler = React.createClass({
  mixins: [GetCurrentUserMixin, Router.Navigation],
  getInitialState: function() {
    return {
      profileId: false
    };
  },
  componentDidMount: function() {
    return this.getCurrentUser();
  },
  componentDidUpdate: function() {
    if (this.state.profileId) {
      return this.transitionTo('/profile/' + this.state.profileId);
    }
  },
  render: function() {
    return React.createElement("div", null, React.createElement("br", null), React.createElement("br", null), React.createElement("h1", {
      "className": "header center red-text"
    }, "Heyo!"), React.createElement("div", {
      "className": "row center"
    }, React.createElement("h5", {
      "className": "header col s12 light"
    }, "The only about me page you\'ll ever need. Trust us.")), React.createElement("div", {
      "className": "row center"
    }, React.createElement("a", {
      "href": "/join/",
      "className": "btn-large waves-effect waves-light red"
    }, "Say Heyo!"), "\u00a0", React.createElement("a", {
      "href": "/login/",
      "className": "btn-large waves-effect waves-light red lighten-1"
    }, "Login")));
  }
});

module.exports = WelcomeHandler;
