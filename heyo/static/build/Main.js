var HeyoRoot, JoinHandler, LoginHandler, ProfileHandler, React, Route, Router, SearchHandler, WelcomeHandler, routes;

React = require("react");

Router = require("react-router");

Route = Router.Route;

HeyoRoot = require("./HeyoRoot.js");

WelcomeHandler = require("./WelcomeHandler.js");

LoginHandler = require("./LoginHandler.js");

JoinHandler = require("./JoinHandler.js");

ProfileHandler = require("./ProfileHandler.js");

SearchHandler = require("./SearchHandler.js");

routes = React.createElement(Route, {
  "handler": HeyoRoot
}, React.createElement(Route, {
  "path": "/",
  "handler": WelcomeHandler
}), React.createElement(Route, {
  "path": "/join/",
  "handler": JoinHandler
}), React.createElement(Route, {
  "path": "/login/",
  "handler": LoginHandler
}), React.createElement(Route, {
  "path": "/search/",
  "handler": SearchHandler
}), React.createElement(Route, {
  "path": "/profile/:profileId",
  "handler": ProfileHandler
}));

Router.run(routes, Router.HistoryLocation, function(Root) {
  return React.render(React.createElement(Root, null), document.body);
});
