var $, React, SearchHandler;

React = require('react');

$ = require('jquery');

SearchHandler = React.createClass({
  getInitialState: function() {
    return {
      profilelist: []
    };
  },
  render: function() {
    return React.createElement("div", {
      "className": "row"
    }, React.createElement("div", {
      "className": "col s12"
    }, React.createElement("ul", {
      "className": "collection with-header"
    }, React.createElement("li", {
      "className": "collection-header"
    }, React.createElement("h5", {
      "className": "red-text lighten-2"
    }, "Search for Users")), React.createElement("li", {
      "className": "collection-item"
    }, React.createElement("input", {
      "type": "text",
      "ref": "query",
      "onChange": this.handleSearch
    })), this.resultNodes())));
  },
  handleSearch: function() {
    var query, that;
    that = this;
    query = $(React.findDOMNode(this.refs.query)).val();
    if (query.length > 2) {
      return $.ajax({
        type: 'GET',
        dataType: 'json',
        url: '/api/searchprofiles/',
        data: {
          query: query
        },
        success: function(profilelist) {
          return that.setState({
            profilelist: profilelist
          });
        },
        error: function(xhr, status, err) {
          return console.log(status, err.toString());
        }
      });
    } else {
      return this.setState({
        profilelist: []
      });
    }
  },
  resultNodes: function() {
    return this.state.profilelist.map(function(profile) {
      return React.createElement("li", {
        "className": "collection-item"
      }, React.createElement("h3", null, React.createElement("a", {
        "href": "/profile/" + profile.id
      }, profile.name)), React.createElement("p", null, profile.work, " \u2022 ", profile.location));
    });
  }
});

module.exports = SearchHandler;
