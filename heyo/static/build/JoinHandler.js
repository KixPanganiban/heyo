var $, Cookies, ErrorMessage, InputGroup, JoinHandler, React, SimpleModal, SuccessMessage, _;

React = require('react');

$ = require('jquery');

_ = require('lodash');

Cookies = require('js-cookie');

SimpleModal = require('./components/SimpleModal.js');

InputGroup = require('./components/InputGroup.js');

ErrorMessage = require('./components/ErrorMessage.js');

SuccessMessage = require('./components/SuccessMessage.js');

JoinHandler = React.createClass({
  getInitialState: function() {
    return {
      showError: false,
      showSuccess: false,
      errorMessage: "",
      profileLink: "",
      fbuser: {
        id: null,
        name: null
      }
    };
  },
  componentDidMount: function() {
    window.fbAsyncInit = function() {
      return FB.init({
        appId: '1630966230450897',
        cookie: true,
        xfbml: true,
        version: 'v2.1'
      });
    };
    return (function(d, s, id) {
      var fjs, js;
      js = void 0;
      fjs = d.getElementsByTagName(s)[0];
      if (d.getElementById(id)) {
        return;
      }
      js = d.createElement('script');
      js.id = id;
      js.src = '//connect.facebook.net/en_US/sdk.js';
      fjs.parentNode.insertBefore(js, fjs);
    })(document, 'body', 'facebook-jssdk');
  },
  checkLoginState: function() {
    return FB.getLoginStatus((function(response) {
      if (response.status === "connected") {
        return FB.api('/me', (function(response) {
          this.setState({
            fbuser: {
              id: response.id,
              name: response.name
            },
            showSuccess: true
          });
          return $(React.findDOMNode(this.refs.nameEntry.refs.input)).val(this.state.fbuser.name);
        }).bind(this));
      }
    }).bind(this));
  },
  handleFBLogin: function() {
    return FB.login(this.checkLoginState());
  },
  render: function() {
    return React.createElement("div", null, React.createElement("div", {
      "className": "row"
    }, React.createElement("h5", {
      "className": "header col s12 light"
    }, "Create your own Heyo! profile")), React.createElement("div", {
      "className": "row"
    }, React.createElement("p", {
      "className": "header col light"
    }, "Joining us is easy and fun! Plus it takes less than 5 minutes. Complete the form below...")), React.createElement(ErrorMessage, {
      "show": this.state.showError,
      "message": this.state.errorMessage
    }), React.createElement(SuccessMessage, {
      "show": this.state.showSuccess,
      "message": "Great! Now just fill in some of the details for your Heyo! profile, and you're done."
    }), React.createElement("div", {
      "className": "row"
    }, React.createElement("form", {
      "className": "col s12",
      "ref": "joinForm",
      "action": "/api/join/",
      "method": "POST"
    }, React.createElement("div", {
      "className": "row"
    }, React.createElement(InputGroup, {
      "placeholder": "francis@mymail.com",
      "formName": "email",
      "type": "email",
      "className": "validate",
      "label": "Email Address"
    }), React.createElement(InputGroup, {
      "placeholder": "password",
      "formName": "password",
      "type": "password",
      "className": "validate",
      "label": "Password"
    })), React.createElement("div", {
      "className": "row"
    }, React.createElement(InputGroup, {
      "ref": "nameEntry",
      "placeholder": "Francis Awesomesauce",
      "formName": "name",
      "type": "text",
      "className": "validate",
      "label": "Name"
    }), React.createElement(InputGroup, {
      "placeholder": "Philippines",
      "formName": "location",
      "type": "text",
      "className": "validate",
      "label": "Location"
    })), React.createElement("div", {
      "className": "row"
    }, React.createElement("div", {
      "className": "input-field col s6"
    }, React.createElement("textarea", {
      "name": "bio",
      "className": "materialize-textarea"
    }), React.createElement("label", {
      "for": "bio"
    }, "Short Bio")), React.createElement(InputGroup, {
      "placeholder": "Code Ninja",
      "formName": "work",
      "type": "text",
      "className": "validate",
      "label": "Work"
    })), React.createElement("div", {
      "className": "row"
    }, React.createElement("input", {
      "type": "hidden",
      "name": "fbuserid",
      "value": this.state.fbuser.id,
      "ref": "fbuserid"
    }), React.createElement("button", {
      "className": "waves-effect waves-light btn red",
      "onClick": this.handleSubmit
    }, "Submit"), "\u00a0", React.createElement("button", {
      "className": "waves-effect waves-light btn blue darken-3",
      "onClick": this.handleFBLogin
    }, "Sign up with Facebook")))), React.createElement(SimpleModal, {
      "ref": "successModal",
      "profileLink": this.state.profileLink,
      "header": "Welcome to Heyo!",
      "text": "Congratulations! You just created your own Heyo! profile. You may now go to your profile and add a profile picture, see your testimonials, or browser other profiles."
    }));
  },
  handleSubmit: function() {
    var joinForm, that;
    that = this;
    joinForm = $(React.findDOMNode(this.refs.joinForm));
    return joinForm.submit(function(e) {
      var formData, obj, _i, _len, _ref;
      formData = {};
      _ref = joinForm.serializeArray();
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        obj = _ref[_i];
        formData[obj.name] = obj.value;
      }
      $.ajax({
        url: joinForm.attr('action'),
        type: 'POST',
        data: formData,
        beforeSend: function(xhr) {
          return xhr.setRequestHeader("X-CSRFToken", Cookies.get("csrftoken"));
        },
        success: function(data) {
          that.handleSuccess(data);
          return console.log(data);
        },
        error: function(xhr, status, err) {
          var errMsg;
          errMsg = err.toString();
          that.handleError(errMsg);
          return console.log(status, errMsg);
        }
      });
      e.preventDefault();
      return e.stopImmediatePropagation();
    });
  },
  handleError: function(errMsg) {
    console.log("Handling error:", errMsg);
    if (errMsg === 'email_taken') {
      return this.setState({
        showError: true,
        errorMessage: "Oops! That email address is already taken. Please choose a different one."
      });
    }
  },
  hideError: function() {
    return this.setState({
      showError: false,
      errorMessage: ""
    });
  },
  handleSuccess: function(profileId) {
    var successModal;
    this.hideError;
    this.setState({
      profileLink: "/profile/" + profileId
    });
    successModal = window.jQuery(React.findDOMNode(this.refs.successModal.refs.modal));
    return successModal.openModal();
  }
});

module.exports = JoinHandler;
