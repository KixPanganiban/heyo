var $, GetCurrentUserMixin, HeyoRoot, React, RouteHandler, Router;

React = require("react");

Router = require("react-router");

$ = require("jquery");

RouteHandler = Router.RouteHandler;

GetCurrentUserMixin = require('./mixins/GetCurrentUserMixin.js');

HeyoRoot = React.createClass({
  mixins: [GetCurrentUserMixin],
  getInitialState: function() {
    return {
      profileId: false
    };
  },
  componentDidMount: function() {
    return this.getCurrentUser();
  },
  render: function() {
    return React.createElement("div", null, React.createElement("nav", {
      "className": "maroon darken-1",
      "role": "navigation"
    }, React.createElement("div", {
      "className": "nav-wrapper container"
    }, React.createElement("a", {
      "id": "logo-container",
      "href": "/",
      "className": "brand-logo"
    }, "Heyo!"), React.createElement("ul", {
      "className": "right hide-on-med-and-down"
    }, this.getUserLinks()), React.createElement("ul", {
      "id": "nav-mobile",
      "className": "side-nav"
    }, this.getUserLinks()), React.createElement("a", {
      "href": "#",
      "data-activates": "nav-mobile",
      "className": "button-collapse"
    }, React.createElement("i", {
      "className": "mdi-navigation-menu"
    })))), React.createElement("div", {
      "className": "section no-pad-bot",
      "id": "index-banner"
    }, React.createElement("div", {
      "className": "container"
    }, React.createElement(RouteHandler, null))));
  },
  getUserLinks: function() {
    var profileLink;
    if (this.state.profileId) {
      profileLink = "/profile/" + this.state.profileId;
      console.log(profileLink);
      return [
        React.createElement("li", null, React.createElement("a", {
          "href": "/search"
        }, "Search")), React.createElement("li", null, React.createElement("a", {
          "href": profileLink
        }, "Profile")), React.createElement("li", null, React.createElement("a", {
          "href": "/logout/"
        }, "Log out"))
      ];
    } else {
      return [
        React.createElement("li", null, React.createElement("a", {
          "href": "/search"
        }, "Search")), React.createElement("li", null, React.createElement("a", {
          "href": "/"
        }, "Home")), React.createElement("li", null, React.createElement("a", {
          "href": "/login/"
        }, "Login"))
      ];
    }
  }
});

module.exports = HeyoRoot;
