var $, Cookies, GetCurrentUserMixin, React, TestimonialEntryView, TestimonialView;

React = require('react');

$ = require('jquery');

Cookies = require('js-cookie');

GetCurrentUserMixin = require('../mixins/GetCurrentUserMixin.js');

TestimonialEntryView = React.createClass({
  render: function() {
    return React.createElement("li", null, React.createElement("form", {
      "className": "col s12",
      "ref": "entryForm",
      "action": "/api/posttestimonial/"
    }, React.createElement("div", {
      "className": "row"
    }, React.createElement("div", {
      "className": "input-field col s10"
    }, React.createElement("textarea", {
      "ref": "entryArea",
      "placeholder": this.renderPlaceholder(),
      "name": "body",
      "className": "materialize-textarea"
    }), React.createElement("input", {
      "type": "hidden",
      "name": "author_id",
      "value": this.props.author_id
    }), React.createElement("input", {
      "type": "hidden",
      "name": "view_profile",
      "value": this.props.view_profile
    }), React.createElement("input", {
      "type": "hidden",
      "name": "target_profile",
      "value": this.props.target_profile
    }), React.createElement("input", {
      "type": "hidden",
      "name": "parent",
      "value": this.props.parent
    })), React.createElement("div", {
      "className": "input-field col s2"
    }, React.createElement("button", {
      "type": "submit",
      "className": "btn waves-effect waves-light red lighten-1",
      "onClick": this.props.handleSubmitTestimonial
    }, "Post")))));
  },
  renderPlaceholder: function() {
    if (this.props.parent) {
      return "Reply to a testimonial:";
    } else {
      return "Write a testimonial:";
    }
  }
});

TestimonialView = React.createClass({
  mixins: [GetCurrentUserMixin],
  getInitialState: function() {
    return {
      testimonials: [],
      target_profile: this.props.target_profile,
      target_testimonial: null,
      profileId: null
    };
  },
  componentDidMount: function() {
    var that;
    that = this;
    this.getCurrentUser();
    return $.ajax({
      url: '/api/getprofiletestimonials/',
      type: 'GET',
      data: {
        id: that.props.target_profile
      },
      dataType: 'json',
      success: function(testimonials) {
        return that.setState({
          testimonials: testimonials
        });
      },
      error: function(xhr, status, err) {
        return console.log(status, err.toString());
      }
    });
  },
  render: function() {
    return React.createElement("div", {
      "className": "row"
    }, React.createElement("div", {
      "className": "col s12"
    }, React.createElement("ul", {
      "className": "collection with-header"
    }, React.createElement("li", {
      "className": "collection-header"
    }, React.createElement("h5", {
      "className": "red-text light"
    }, "User Testimonials")), this.renderTestimonials(), this.renderEntry())));
  },
  renderTestimonials: function() {
    if (this.state.testimonials.length > 0) {
      return this.state.testimonials.map((function(testimonial) {
        return React.createElement("li", {
          "className": "collection-item"
        }, React.createElement("div", {
          "className": "section"
        }, React.createElement("a", {
          "href": "/profile/" + testimonial.author.id
        }, React.createElement("b", null, testimonial.author.name)), React.createElement("br", null), React.createElement("p", null, testimonial.body, " ", this.renderReplyButton(testimonial.id)), this.renderReplies(testimonial.replies)));
      }).bind(this));
    } else {
      return React.createElement("li", {
        "className": "collection-item"
      }, React.createElement("p", null, "No testimonials for this user yet!"));
    }
  },
  renderReplies: function(replies) {
    return replies.map(function(reply) {
      return React.createElement("div", null, React.createElement("small", null, React.createElement("span", {
        "className": "mdi-navigation-arrow-forward"
      }), "\u00a0", React.createElement("a", {
        "href": "/profile/" + reply.author.id
      }, React.createElement("b", null, reply.author.name)), ": ", reply.body));
    });
  },
  renderEntry: function() {
    if (this.state.profileId) {
      return React.createElement(TestimonialEntryView, {
        "ref": "entryView",
        "author_id": this.state.profileId,
        "target_profile": this.state.target_profile,
        "parent": this.state.target_testimonial,
        "view_profile": this.props.viewProfileId,
        "handleSubmitTestimonial": this.handleSubmitTestimonial
      });
    }
  },
  renderReplyButton: function(testimonial_id) {
    if (this.state.profileId) {
      return React.createElement("span", null, " \u2022 ", React.createElement("a", {
        "href": "javascript:;",
        "onClick": this.handleReply.bind(this, testimonial_id)
      }, "Reply"));
    }
  },
  handleSubmitTestimonial: function(e) {
    var entryForm, that;
    that = this;
    entryForm = $(React.findDOMNode(this.refs.entryView.refs.entryForm));
    return entryForm.submit(function(e) {
      var formData, obj, _i, _len, _ref;
      formData = {};
      _ref = entryForm.serializeArray();
      for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        obj = _ref[_i];
        if (obj.value.length > 0) {
          formData[obj.name] = obj.value;
        }
      }
      $.ajax({
        url: entryForm.attr('action'),
        type: 'POST',
        dataType: 'json',
        data: formData,
        beforeSend: function(xhr) {
          return xhr.setRequestHeader("X-CSRFToken", Cookies.get("csrftoken"));
        },
        success: function(testimonials) {
          that.setState({
            testimonials: testimonials,
            target_profile: that.props.target_profile,
            target_testimonial: null
          });
          return entryForm.trigger('reset');
        },
        error: function(xhr, status, err) {
          return console.log(status, err.toString());
        }
      });
      e.preventDefault();
      return e.stopImmediatePropagation();
    });
  },
  handleReply: function(parentId) {
    var entryArea;
    this.setState({
      target_testimonial: parentId,
      target_profile: null
    });
    entryArea = $(React.findDOMNode(this.refs.entryView.refs.entryArea));
    return entryArea.focus();
  }
});

module.exports = TestimonialView;
