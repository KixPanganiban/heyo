var InputGroup, React;

React = require('react');

InputGroup = React.createClass({
  render: function() {
    return React.createElement("div", {
      "className": "input-field col s6"
    }, React.createElement("input", {
      "ref": "input",
      "placeholder": this.props.placeholder,
      "name": this.props.formName,
      "type": this.props.type,
      "className": this.props.className,
      "required": true
    }), React.createElement("label", {
      "for": this.props.formName,
      "className": "active"
    }, this.props.label));
  }
});

module.exports = InputGroup;
