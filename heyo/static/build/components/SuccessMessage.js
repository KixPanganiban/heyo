var React, SuccessMessage;

React = require('react');

SuccessMessage = React.createClass({
  render: function() {
    return React.createElement("div", {
      "className": "row",
      "className": this.getShowProps()
    }, React.createElement("div", {
      "className": "col s12 m5"
    }, React.createElement("div", {
      "className": "card-panel green"
    }, React.createElement("span", {
      "className": "white-text"
    }, this.props.message))));
  },
  getShowProps: function() {
    if (this.props.show === false) {
      return "hide";
    }
    return "";
  }
});

module.exports = SuccessMessage;
