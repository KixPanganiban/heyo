var $, Cookies, PhotoModal, React;

React = require('react');

$ = require('jquery');

Cookies = require('js-cookie');

PhotoModal = React.createClass({
  render: function() {
    return React.createElement("div", {
      "ref": "modal",
      "className": "modal modal-fixed-footer"
    }, React.createElement("div", {
      "className": "modal-content"
    }, React.createElement("img", {
      "src": this.props.photoUrl,
      "className": "responsive-img"
    })), this.renderSetProfilePhoto());
  },
  renderSetProfilePhoto: function() {
    if (!this.props.photoOwner()) {
      return React.createElement("div", {
        "className": "modal-footer"
      }, React.createElement("a", {
        "href": "javascript:;",
        "className": "modal-action modal-close waves-effect waves-green btn-flat"
      }, "Close"));
    } else {
      return React.createElement("div", {
        "className": "modal-footer"
      }, React.createElement("a", {
        "href": "javascript:;",
        "className": "modal-action modal-close waves-effect waves-green btn-flat",
        "onClick": this.handleSetProfilePhoto
      }, "Set As Profile Picture"), React.createElement("a", {
        "href": "javascript:;",
        "className": "modal-action modal-close waves-effect waves-green btn-flat"
      }, "Close"));
    }
  },
  handleSetProfilePhoto: function() {
    return $.ajax({
      url: '/api/setprofilephoto/',
      type: 'POST',
      dataType: 'json',
      beforeSend: function(xhr) {
        return xhr.setRequestHeader("X-CSRFToken", Cookies.get("csrftoken"));
      },
      data: {
        id: this.props.photoId
      },
      success: function(response) {
        if (response.status === "ok") {
          return window.location.reload();
        }
      },
      error: function(xhr, status, err) {
        return console.log(status, err.toString());
      }
    });
  }
});

module.exports = PhotoModal;
