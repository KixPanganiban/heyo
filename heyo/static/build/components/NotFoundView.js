var NotFoundView, React;

React = require('react');

NotFoundView = React.createClass({
  render: function() {
    return React.createElement("div", {
      "className": "row"
    }, React.createElement("div", {
      "className": "col s12"
    }, React.createElement("div", {
      "className": "card"
    }, React.createElement("div", {
      "className": "card-content"
    }, React.createElement("span", {
      "className": "card-title black-text"
    }, "Page Not Found"), React.createElement("p", null, "Oops, it seems that you followed a broken link. That page does not exist.")), React.createElement("div", {
      "className": "card-action"
    }, React.createElement("a", {
      "href": "/#/"
    }, "Go Home")))));
  }
});

module.exports = NotFoundView;
