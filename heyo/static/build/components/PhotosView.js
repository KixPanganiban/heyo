var $, Cookies, GetCurrentUserMixin, PhotoModal, PhotoUploadView, PhotosView, React;

React = require('react');

$ = require('jquery');

Cookies = require('js-cookie');

PhotoModal = require('./PhotoModal.js');

GetCurrentUserMixin = require('../mixins/GetCurrentUserMixin.js');

PhotoUploadView = React.createClass({
  render: function() {
    return React.createElement("li", null, React.createElement("form", {
      "className": "col s10",
      "ref": "photoForm",
      "action": "/api/uploadphoto/",
      "encType": "multipart/form-data",
      "method": "POST"
    }, React.createElement("div", {
      "className": "file-field input-field col s9 offset-s1"
    }, React.createElement("input", {
      "className": "file-path validate",
      "type": "text"
    }), React.createElement("div", {
      "className": "btn"
    }, React.createElement("span", null, "File"), React.createElement("input", {
      "type": "file",
      "name": "file",
      "accept": "image/*"
    }))), React.createElement("div", {
      "className": "input-field col s2"
    }, React.createElement("button", {
      "type": "submit",
      "className": "btn waves-effect waves-light red lighten-1"
    }, "Upload")), React.createElement("input", {
      "type": "hidden",
      "name": "csrfmiddlewaretoken",
      "value": this.csrfToken()
    })));
  },
  csrfToken: function() {
    return Cookies.get("csrftoken");
  }
});

PhotosView = React.createClass({
  mixins: [GetCurrentUserMixin],
  getInitialState: function() {
    return {
      photos: [],
      showAll: false,
      profileId: null,
      viewPhotoUrl: null,
      viewPhotoId: null
    };
  },
  componentDidMount: function() {
    var that;
    that = this;
    this.getCurrentUser();
    return $.ajax({
      url: '/api/getprofilephotos/',
      data: {
        id: that.props.profileId
      },
      dataType: 'json',
      success: function(photos) {
        return that.setState({
          photos: photos
        });
      },
      error: function(xhr, status, err) {
        return console.log(status, err.toString());
      }
    });
  },
  render: function() {
    return React.createElement("div", {
      "className": "row"
    }, React.createElement("div", {
      "className": "col s12"
    }, React.createElement("ul", {
      "className": "collection with-header"
    }, React.createElement("li", {
      "className": "collection-header"
    }, React.createElement("h5", {
      "className": "red-text light"
    }, "User Photos"), this.renderShowAllLink()), this.renderPhotoUploadView(), React.createElement("li", {
      "className": "collection-item"
    }, this.renderPhotos()))), React.createElement(PhotoModal, {
      "ref": "photoModal",
      "photoOwner": this.photoOwner,
      "photoUrl": this.state.viewPhotoUrl,
      "photoId": this.state.viewPhotoId
    }));
  },
  photoOwner: function() {
    return this.state.profileId && (this.state.profileId === this.props.profileId);
  },
  renderShowAllLink: function() {
    if ((this.state.photos.length > 4) && !this.state.showAll) {
      return React.createElement("a", {
        "href": "javascript:;",
        "onClick": this.handleShowAll
      }, "Show all");
    }
  },
  renderPhotos: function() {
    var counter, that;
    that = this;
    if (this.state.photos.length > 0) {
      if (this.state.showAll) {
        return this.state.photos.map(function(photo) {
          return React.createElement("div", {
            "className": "col s3"
          }, React.createElement("a", {
            "href": "javascript:;",
            "onClick": that.handlePhotoShow.bind(this, photo.id, photo.url)
          }, React.createElement("img", {
            "src": photo.url,
            "className": "responsive-img"
          })));
        });
      } else {
        counter = 0;
        return this.state.photos.map(function(photo) {
          if (counter < 4) {
            counter += 1;
            return React.createElement("div", {
              "className": "col s3"
            }, React.createElement("a", {
              "href": "javascript:;",
              "onClick": that.handlePhotoShow.bind(this, photo.id, photo.url)
            }, React.createElement("img", {
              "src": photo.url,
              "className": "responsive-img"
            })));
          }
        });
      }
    } else {
      if (!(this.state.profileId === this.props.profileId)) {
        return React.createElement("p", null, "This user has not uploaded any photos yet.");
      }
    }
  },
  renderPhotoUploadView: function() {
    if (this.photoOwner()) {
      return React.createElement(PhotoUploadView, null);
    }
  },
  handleShowAll: function() {
    return this.setState({
      showAll: true
    });
  },
  handlePhotoShow: function(photoId, photoUrl) {
    var photoModal;
    this.setState({
      viewPhotoId: photoId,
      viewPhotoUrl: photoUrl
    });
    photoModal = window.jQuery(React.findDOMNode(this.refs.photoModal.refs.modal));
    return photoModal.openModal();
  }
});

module.exports = PhotosView;
