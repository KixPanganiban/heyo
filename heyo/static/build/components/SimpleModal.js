var React, SimpleModal;

React = require("react");

SimpleModal = React.createClass({
  render: function() {
    return React.createElement("div", {
      "ref": "modal",
      "className": "modal modal-fixed-footer"
    }, React.createElement("div", {
      "className": "modal-content"
    }, React.createElement("h4", null, this.props.header), React.createElement("p", null, this.props.text)), React.createElement("div", {
      "className": "modal-footer"
    }, React.createElement("a", {
      "href": this.props.profileLink,
      "className": "modal-action modal-close waves-effect waves-green btn-flat "
    }, "Go to Profile")));
  }
});

module.exports = SimpleModal;
