React = require 'react'
$ = require 'jquery'
_ = require 'lodash'
Cookies = require 'js-cookie'
SimpleModal = require './components/SimpleModal.js'
InputGroup = require './components/InputGroup.js'
ErrorMessage = require './components/ErrorMessage.js'
SuccessMessage = require './components/SuccessMessage.js'

JoinHandler = React.createClass
	getInitialState: () ->
		return {
			showError: false
			showSuccess: false
			errorMessage: ""
			profileLink: ""
			fbuser: {
				id: null
				name: null
			}
		}
	componentDidMount: () ->
		# Initialize Facebook SDK
		window.fbAsyncInit = () ->
			FB.init {
				appId: '1630966230450897'
				cookie: true
				xfbml: true
				version: 'v2.1'
			}

		((d, s, id) ->
			js = undefined
			fjs = d.getElementsByTagName(s)[0]
			if d.getElementById(id)
				return
			js = d.createElement('script')
			js.id = id
			js.src = '//connect.facebook.net/en_US/sdk.js'
			fjs.parentNode.insertBefore js, fjs
			return
		) document, 'body', 'facebook-jssdk'
	checkLoginState: () ->
		FB.getLoginStatus ((response) ->
			if response.status is "connected"
				FB.api '/me', ((response) ->
					this.setState {
						fbuser: {
							id: response.id
							name: response.name
						}
						showSuccess: true
					}
					$(React.findDOMNode(this.refs.nameEntry.refs.input)).val(this.state.fbuser.name)
				).bind this		
		).bind this
	handleFBLogin: () ->
		FB.login this.checkLoginState()
	render: () ->
		<div>
			<div className="row">
				<h5 className="header col s12 light">Create your own Heyo! profile</h5>
			</div>
			<div className="row">
				<p className="header col light">Joining us is easy and fun! Plus it takes less than 5 minutes. Complete the form below...</p>
			</div>
			<ErrorMessage show={this.state.showError} message={this.state.errorMessage}/>
			<SuccessMessage show={this.state.showSuccess} message="Great! Now just fill in some of the details for your Heyo! profile, and you're done."/>
			<div className="row">
				<form className="col s12" ref="joinForm" action="/api/join/" method="POST">
					<div className="row">
						<InputGroup placeholder="francis@mymail.com" formName="email" type="email" className="validate" label="Email Address"/>
						<InputGroup placeholder="password" formName="password" type="password" className="validate" label="Password"/>
					</div>
					<div className="row">
						<InputGroup ref="nameEntry" placeholder="Francis Awesomesauce" formName="name" type="text" className="validate" label="Name"/>
						<InputGroup placeholder="Philippines" formName="location" type="text" className="validate" label="Location"/>
					</div>
					<div className="row">
						<div className="input-field col s6">
							<textarea name="bio" className="materialize-textarea"></textarea>
							<label for="bio">Short Bio</label>
						</div>
						<InputGroup placeholder="Code Ninja" formName="work" type="text" className="validate" label="Work"/>
					</div>
					<div className="row">
						<input type="hidden" name="fbuserid" value={this.state.fbuser.id} ref="fbuserid" />
						<button className="waves-effect waves-light btn red" onClick={this.handleSubmit}>Submit</button>&nbsp;
						<button className="waves-effect waves-light btn blue darken-3" onClick={this.handleFBLogin}>Sign up with Facebook</button>
					</div>
				</form>
			</div>
			<SimpleModal ref="successModal" profileLink={this.state.profileLink} header="Welcome to Heyo!" text="Congratulations! You just created your own Heyo! profile. You may now go to your profile and add a profile picture, see your testimonials, or browser other profiles."/>
		</div>
	handleSubmit: () ->
		that = this
		joinForm = $(React.findDOMNode(this.refs.joinForm))
		joinForm.submit (e) ->
			formData = {}
			for obj in joinForm.serializeArray()
				formData[obj.name] = obj.value

			$.ajax {
				url: joinForm.attr 'action'
				type: 'POST'
				data: formData
				beforeSend: (xhr) ->
					xhr.setRequestHeader "X-CSRFToken", Cookies.get("csrftoken")
				success: (data) ->
					that.handleSuccess(data)
					console.log data
				error: (xhr, status, err) ->
					errMsg = err.toString()
					that.handleError errMsg
					console.log status, errMsg
			}
			e.preventDefault()
			e.stopImmediatePropagation()
	handleError: (errMsg) ->
		console.log "Handling error:", errMsg
		if errMsg is 'email_taken'
			this.setState {
				showError: true
				errorMessage: "Oops! That email address is already taken. Please choose a different one."
			}
	hideError: () ->
		this.setState {
			showError: false
			errorMessage: ""
		}
	handleSuccess: (profileId) ->
		this.hideError
		this.setState {
			profileLink: "/profile/" + profileId
		}
		# Use window.jQuery to pass handling to browser-loaded jQuery with Materialize
		successModal = window.jQuery(React.findDOMNode(this.refs.successModal.refs.modal))
		successModal.openModal()

module.exports = JoinHandler