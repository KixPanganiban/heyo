React = require 'react'
$ = require 'jquery'
Cookies = require 'js-cookie'

GetCurrentUserMixin = require './mixins/GetCurrentUserMixin.js'

NotFoundView = require './components/NotFoundView.js'
TestimonialView = require './components/TestimonialView.js'
PhotosView = require './components/PhotosView.js'

ProfileBodyView = React.createClass
	render: () ->
		<div>
			<div className="row">
				<div className="col s12">
					<ul className="collection with-header">
						<li className="collection-header">
							<div className="row">
								<div className="col s3">
									<img src={this.props.viewProfileImage} className="responsive-img circle"/>
								</div>
								<div className="col s9">
									<h3 className="red-text light">{this.props.viewProfileData.name}</h3>
									<p className="light">{this.props.viewProfileData.bio}</p>
								</div>
							</div>
						</li>
						<li className="collection-item"><i className="mdi-maps-pin-drop">&nbsp;{this.props.viewProfileData.location}</i></li>
						<li className="collection-item"><i className="mdi-action-wallet-travel">&nbsp;{this.props.viewProfileData.work}</i></li>
					</ul>
				</div>
			</div>
			<PhotosView profileId={this.props.viewProfileId} />
			<TestimonialView target_profile={this.props.viewProfileId} viewProfileId={this.props.viewProfileId}/>
		</div>

ProfileHandler = React.createClass
	getInitialState: () ->
		{
			viewProfileData: {}
			viewProfileImage: ""
			viewProfileId: this.props.params.profileId
			status: "not_loaded"
		}
	componentDidMount: () ->
		that = this 
		$.ajax {
			url: '/api/profile/'
			dataType: 'json'
			type: 'GET'
			data: {
				id: this.props.params.profileId 
			}
			success: ((viewProfileData) ->
				this.setState {
					viewProfileData: viewProfileData
					status: "loaded"
					viewProfileImage: viewProfileData.profile_photo
				}
				).bind(this)
			error: ((xhr, status, err) ->
				this.setState {
					status: "not_found"
				}
				console.log status, err.toString()
				).bind(this)
		}
	renderByStatus: () ->
		if this.state.status is "loaded"
			return <ProfileBodyView viewProfileId={this.state.viewProfileId} viewProfileData={this.state.viewProfileData} viewProfileImage={this.state.viewProfileImage}/>
		else if this.state.status is "not_found"
			return <NotFoundView/>

	render: () ->
		<div>
			{this.renderByStatus()}
		</div>

module.exports = ProfileHandler