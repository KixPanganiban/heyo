React = require 'react'
Router = require 'react-router'
$ = require 'jquery'
Cookies = require 'js-cookie'

InputGroup = require './components/InputGroup.js'
ErrorMessage = require './components/ErrorMessage.js'

LoginHandler = React.createClass
	mixins: [Router.Navigation]
	getInitialState: () ->
		{
			showError: false
			errorMessage: "Hey"
		}
	componentDidMount: () ->
		# Initialize Facebook SDK
		window.fbAsyncInit = () ->
			FB.init {
				appId: '1630966230450897'
				cookie: true
				xfbml: true
				version: 'v2.1'
			}

		((d, s, id) ->
			js = undefined
			fjs = d.getElementsByTagName(s)[0]
			if d.getElementById(id)
				return
			js = d.createElement('script')
			js.id = id
			js.src = '//connect.facebook.net/en_US/sdk.js'
			fjs.parentNode.insertBefore js, fjs
			return
		) document, 'body', 'facebook-jssdk'
	checkLoginState: () ->
		that = this
		FB.getLoginStatus ((response) ->
			if response.status is "connected"
				FB.api '/me', ((response) ->
					$.ajax {
						url: '/api/facebooklogin/'
						type: 'POST'
						data: {
							id: response.id
						}
						beforeSend: (xhr) ->
							xhr.setRequestHeader "X-CSRFToken", Cookies.get("csrftoken")
						success: (profileId) ->
							that.transitionTo '/profile/' + profileId
						error: (xhr, status, err) ->
							errMsg = err.toString()
							that.handleError errMsg
							console.log status, errMsg
					}
				).bind this		
		).bind this
	handleFBLogin: () ->
		FB.login this.checkLoginState()
	render: () ->
		<div>
			<div className="row">
				<h5 className="header col s12 light">Welcome back!</h5>
			</div>
			<div className="row">
				<p className="header col light">Enter your login details, or login with Facebook.</p>
			</div>
			<ErrorMessage show={this.state.showError} message={this.state.errorMessage}/>
			<div className="row">
				<form className="col s12" ref="loginForm" action="/api/login/" method="POST">
					<div className="row">
						<InputGroup placeholder="francis@mymail.com" formName="email" type="email" className="validate" label="Email Address"/>
						<InputGroup placeholder="password" formName="password" type="password" className="validate" label="Password"/>
					</div>
					<div className="row">
						<button className="waves-effect waves-light btn red" onClick={this.handleSubmit}>Login</button>&nbsp;
						<button className="waves-effect waves-light btn blue darken-3" onClick={this.handleFBLogin}>Login with Facebook</button>
					</div>
				</form>
			</div>
		</div>
	handleSubmit: () ->
		that = this
		loginForm = $(React.findDOMNode(this.refs.loginForm))
		console.log loginForm.attr('action')
		loginForm.submit (e) ->
			formData = {}
			for obj in loginForm.serializeArray()
				formData[obj.name] = obj.value

			$.ajax {
				url: loginForm.attr 'action'
				type: 'POST'
				data: formData
				beforeSend: (xhr) ->
					xhr.setRequestHeader "X-CSRFToken", Cookies.get("csrftoken")
				success: (profileId) ->
					that.transitionTo '/profile/' + profileId
				error: (xhr, status, err) ->
					errMsg = err.toString()
					that.handleError errMsg
					console.log status, errMsg
			}
			e.preventDefault()
			e.stopImmediatePropagation()

	handleError: (errMsg) ->
		console.log "Handling error:", errMsg
		if errMsg is 'auth_fail'
			this.setState {
				showError: true
				errorMessage: "Oops! Invalid email address and/or password. Please re-check your details and try again."
			}
		else if errMsg is 'no_fb_user'
			this.setState {
				showError: true
				errorMessage: "Oops! That Facebook user isn't registered in Heyo! yet."
			}

module.exports = LoginHandler