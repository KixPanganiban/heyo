GetCurrentUserMixin = {
	getCurrentUser: () ->
		$.ajax {
			url: '/api/getcurrentuser/'
			type: 'GET'
			success: ((profileId) ->
				this.setState {
					profileId: profileId
				}
				).bind(this)
			error: ((xhr, status, err) ->
				this.setState {
					profileId: false
				}
				console.log status, err.toString()
				).bind(this)			
		}
}

module.exports = GetCurrentUserMixin