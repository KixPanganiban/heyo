# Initialize ReactJS and react-router
React = require "react"
Router = require "react-router"
Route = Router.Route
HeyoRoot = require "./HeyoRoot.js"

# Load Handlers
WelcomeHandler = require "./WelcomeHandler.js"
LoginHandler = require "./LoginHandler.js"
JoinHandler = require "./JoinHandler.js"
ProfileHandler = require "./ProfileHandler.js"
SearchHandler = require "./SearchHandler.js"
routes = (
	<Route handler={HeyoRoot}>
		<Route path="/" handler={WelcomeHandler}/>
		<Route path="/join/" handler={JoinHandler}/>
		<Route path="/login/" handler={LoginHandler}/>
		<Route path="/search/" handler={SearchHandler}/>
		<Route path="/profile/:profileId" handler={ProfileHandler}/>
	</Route>
	)

Router.run routes, Router.HistoryLocation, (Root) -> React.render(<Root/>, document.body)
