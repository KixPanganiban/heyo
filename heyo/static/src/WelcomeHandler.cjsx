React = require "react"
Router = require "react-router"
GetCurrentUserMixin = require './mixins/GetCurrentUserMixin.js'

WelcomeHandler = React.createClass
	mixins: [GetCurrentUserMixin, Router.Navigation]
	getInitialState: () ->
		{
			profileId: false
		}
	componentDidMount: () ->
		this.getCurrentUser()
	componentDidUpdate: () ->
		if this.state.profileId
			this.transitionTo '/profile/'+this.state.profileId
	render: () ->
		<div>
			<br/><br/>
			<h1 className="header center red-text">Heyo!</h1>
			<div className="row center">
				<h5 className="header col s12 light">The only about me page you'll ever need. Trust us.</h5>
			</div>
			<div className="row center">
				<a href="/join/" className="btn-large waves-effect waves-light red">Say Heyo!</a>&nbsp;
				<a href="/login/" className="btn-large waves-effect waves-light red lighten-1">Login</a>
			</div>
		</div>
module.exports = WelcomeHandler