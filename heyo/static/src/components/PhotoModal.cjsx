React = require 'react'
$ = require 'jquery'
Cookies = require 'js-cookie'

PhotoModal = React.createClass
	render: () ->
		<div ref="modal" className="modal modal-fixed-footer">
			<div className="modal-content">
				<img src={this.props.photoUrl} className="responsive-img"/>
			</div>
			{this.renderSetProfilePhoto()}

		</div>
	renderSetProfilePhoto: () ->
		if not this.props.photoOwner()
			<div className="modal-footer">
				<a href="javascript:;" className="modal-action modal-close waves-effect waves-green btn-flat">Close</a>
			</div>
		else
			<div className="modal-footer">
				<a href="javascript:;" className="modal-action modal-close waves-effect waves-green btn-flat" onClick={this.handleSetProfilePhoto}>Set As Profile Picture</a>
				<a href="javascript:;" className="modal-action modal-close waves-effect waves-green btn-flat">Close</a>
			</div>
	handleSetProfilePhoto: () ->
		$.ajax {
			url: '/api/setprofilephoto/'
			type: 'POST'
			dataType: 'json'
			beforeSend: (xhr) ->
				xhr.setRequestHeader "X-CSRFToken", Cookies.get("csrftoken")
			data: {
				id: this.props.photoId
			}
			success: (response) ->
				if response.status is "ok"
					window.location.reload()
			error: (xhr, status, err) ->
				console.log status, err.toString()
		}

module.exports = PhotoModal