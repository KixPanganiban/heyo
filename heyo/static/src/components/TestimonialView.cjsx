React = require 'react'
$ = require 'jquery'
Cookies = require 'js-cookie'

GetCurrentUserMixin = require '../mixins/GetCurrentUserMixin.js'

TestimonialEntryView = React.createClass
	render: () ->
		<li>
			<form className="col s12" ref="entryForm" action="/api/posttestimonial/">
				<div className="row">
					<div className="input-field col s10">
						<textarea ref="entryArea" placeholder={this.renderPlaceholder()} name="body" className="materialize-textarea"></textarea>
						<input type="hidden" name="author_id" value={this.props.author_id} />
						<input type="hidden" name="view_profile" value={this.props.view_profile}/>
						<input type="hidden" name="target_profile" value={this.props.target_profile}/>
						<input type="hidden" name="parent" value={this.props.parent}/>
					</div>
					<div className="input-field col s2">
						<button type="submit" className="btn waves-effect waves-light red lighten-1" onClick={this.props.handleSubmitTestimonial}>Post</button>
					</div>
				</div>
			</form>	
		</li>
	renderPlaceholder: () ->
		if this.props.parent
			"Reply to a testimonial:"
		else
			"Write a testimonial:"

TestimonialView = React.createClass
	mixins: [GetCurrentUserMixin]
	getInitialState: () ->
		{
			testimonials: []
			target_profile: this.props.target_profile
			target_testimonial: null
			profileId: null
		}
	componentDidMount: () ->
		that = this
		this.getCurrentUser()
		$.ajax {
			url: '/api/getprofiletestimonials/'
			type: 'GET'
			data: {
				id: that.props.target_profile
			}
			dataType: 'json'
			success: (testimonials) ->
				that.setState {testimonials: testimonials}
			error: (xhr, status, err) ->
				console.log status, err.toString()
		}
	render: () ->
		<div className="row">
			<div className="col s12">
				<ul className="collection with-header">
					<li className="collection-header">
						<h5 className="red-text light">User Testimonials</h5>
					</li>
					{this.renderTestimonials()}
					{this.renderEntry()}
				</ul>
			</div>
		</div>
	renderTestimonials: () ->
		if this.state.testimonials.length > 0
			this.state.testimonials.map ((testimonial) ->
				<li className="collection-item">
					<div className="section">
						<a href={"/profile/" + testimonial.author.id}><b>{testimonial.author.name}</b></a>
						<br/>
						<p>{testimonial.body} {this.renderReplyButton(testimonial.id)}</p>
						{this.renderReplies(testimonial.replies)}
					</div>
				</li>
				).bind(this)
		else
			<li className="collection-item">
				<p>No testimonials for this user yet!</p>
			</li>
	renderReplies: (replies) ->
		replies.map (reply) ->
			<div>
				<small>
					<span className="mdi-navigation-arrow-forward"></span>&nbsp;<a href={"/profile/" + reply.author.id}><b>{reply.author.name}</b></a>: {reply.body}
				</small>
			</div>
	renderEntry: () ->
		if this.state.profileId
			<TestimonialEntryView ref="entryView" author_id={this.state.profileId} target_profile={this.state.target_profile} parent={this.state.target_testimonial} view_profile={this.props.viewProfileId} handleSubmitTestimonial={this.handleSubmitTestimonial}/>
	renderReplyButton: (testimonial_id) ->
		if this.state.profileId
			<span> &bull; <a href="javascript:;" onClick={this.handleReply.bind(this, testimonial_id)}>Reply</a></span>
	handleSubmitTestimonial: (e) ->
		that = this
		entryForm = $(React.findDOMNode(this.refs.entryView.refs.entryForm))
		entryForm.submit (e) ->
			formData = {}
			for obj in entryForm.serializeArray()
				if obj.value.length > 0
					formData[obj.name] = obj.value

			$.ajax {
				url: entryForm.attr 'action'
				type: 'POST'
				dataType: 'json'
				data: formData
				beforeSend: (xhr) ->
					xhr.setRequestHeader "X-CSRFToken", Cookies.get("csrftoken")
				success: (testimonials) ->
					that.setState {
						testimonials: testimonials
						target_profile: that.props.target_profile
						target_testimonial: null
					}
					entryForm.trigger('reset')
				error: (xhr, status, err) ->
					console.log status, err.toString()
			}

			e.preventDefault()
			e.stopImmediatePropagation()
	handleReply: (parentId) ->
		this.setState {
			target_testimonial: parentId
			target_profile: null
		}
		entryArea = $(React.findDOMNode(this.refs.entryView.refs.entryArea))
		entryArea.focus()

module.exports = TestimonialView