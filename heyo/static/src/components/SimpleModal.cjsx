React = require "react"

SimpleModal = React.createClass
	render: () ->
		<div ref="modal" className="modal modal-fixed-footer">
			<div className="modal-content">
				<h4>{this.props.header}</h4>
				<p>{this.props.text}</p>
			</div>
			<div className="modal-footer">
				<a href={this.props.profileLink} className="modal-action modal-close waves-effect waves-green btn-flat ">Go to Profile</a>
			</div>
		</div>

module.exports = SimpleModal