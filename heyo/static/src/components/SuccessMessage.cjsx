React = require 'react'

SuccessMessage = React.createClass
	render: () ->
		<div className="row" className={this.getShowProps()}>
			<div className="col s12 m5">
				<div className="card-panel green">
					<span className="white-text">
						{this.props.message}
					</span>
				</div>
			</div>
		</div>
	getShowProps: () ->
		if this.props.show is false
			return "hide"
		return ""

module.exports = SuccessMessage