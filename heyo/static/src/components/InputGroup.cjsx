React = require 'react'

InputGroup = React.createClass
	render: () ->
		<div className="input-field col s6">
			<input ref="input" placeholder={this.props.placeholder} name={this.props.formName} type={this.props.type} className={this.props.className} required/>
			<label for={this.props.formName} className="active">{this.props.label}</label>
		</div>

module.exports = InputGroup