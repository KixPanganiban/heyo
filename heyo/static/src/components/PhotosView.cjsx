React = require 'react'
$ = require 'jquery'
Cookies = require 'js-cookie'


PhotoModal = require './PhotoModal.js'
GetCurrentUserMixin = require '../mixins/GetCurrentUserMixin.js'


PhotoUploadView = React.createClass
	render: () ->
		<li>
			<form className="col s10" ref="photoForm" action="/api/uploadphoto/" encType="multipart/form-data" method="POST">
					<div className="file-field input-field col s9 offset-s1">
						<input className="file-path validate" type="text" />
						<div className="btn">
							<span>File</span>
							<input type="file" name="file" accept="image/*"/>
						</div>
					</div>
					<div className="input-field col s2">
						<button type="submit" className="btn waves-effect waves-light red lighten-1">Upload</button>
					</div>
					<input type="hidden" name="csrfmiddlewaretoken" value={this.csrfToken()}/>
			</form>	
		</li>
	csrfToken: () ->
		Cookies.get("csrftoken")

PhotosView = React.createClass
	mixins: [GetCurrentUserMixin]
	getInitialState: () ->
		{
			photos: []
			showAll: false
			profileId: null
			viewPhotoUrl: null
			viewPhotoId: null
		}
	componentDidMount: () ->
		that = this
		this.getCurrentUser()
		$.ajax {
			url: '/api/getprofilephotos/'
			data: {
				id: that.props.profileId 
			}
			dataType: 'json'
			success: (photos) ->
				that.setState {
					photos: photos
				}
			error: (xhr, status, err) ->
				console.log status, err.toString()
		}
	render: () ->
		<div className="row">
			<div className="col s12">
				<ul className="collection with-header">
					<li className="collection-header">
						<h5 className="red-text light">User Photos</h5>
						{this.renderShowAllLink()}
					</li>
					{this.renderPhotoUploadView()}
					<li className="collection-item">
						{this.renderPhotos()}
					</li>
				</ul>
			</div>
			<PhotoModal ref="photoModal" photoOwner={this.photoOwner} photoUrl={this.state.viewPhotoUrl} photoId={this.state.viewPhotoId}/>
		</div>
	photoOwner: () ->
		this.state.profileId and (this.state.profileId is this.props.profileId)
	renderShowAllLink: () ->
		if (this.state.photos.length > 4) and not this.state.showAll
			<a href="javascript:;" onClick={this.handleShowAll}>Show all</a>

	renderPhotos: () ->
		that = this
		if this.state.photos.length > 0
			if this.state.showAll
				this.state.photos.map (photo) ->
					<div className="col s3">
						<a href="javascript:;" onClick={that.handlePhotoShow.bind(this,photo.id,photo.url)}>
							<img src={photo.url} className="responsive-img"/>
						</a>
					</div>
			else
				counter = 0
				this.state.photos.map (photo) ->
					if counter < 4
						counter += 1
						<div className="col s3">
							<a href="javascript:;" onClick={that.handlePhotoShow.bind(this,photo.id,photo.url)}>
								<img src={photo.url} className="responsive-img"/>
							</a>
						</div>
		else
			if not (this.state.profileId is this.props.profileId)
				<p>This user has not uploaded any photos yet.</p>

	renderPhotoUploadView: () ->
		if this.photoOwner()
			<PhotoUploadView/>
	handleShowAll: () ->
		this.setState {
			showAll: true
		}
	handlePhotoShow: (photoId,photoUrl) ->
		this.setState {
			viewPhotoId: photoId
			viewPhotoUrl: photoUrl
		}
		photoModal = window.jQuery(React.findDOMNode(this.refs.photoModal.refs.modal))
		photoModal.openModal()

module.exports = PhotosView