React = require 'react'

NotFoundView = React.createClass
	render: () ->
		<div className="row">
			<div className="col s12">
				<div className="card">
					<div className="card-content">
						<span className="card-title black-text">Page Not Found</span>
						<p>Oops, it seems that you followed a broken link. That page does not exist.</p>
					</div>
					<div className="card-action">
						<a href="/#/">Go Home</a>
					</div>
				</div>
			</div>
		</div>

module.exports = NotFoundView