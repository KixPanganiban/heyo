React = require 'react'
$ = require 'jquery'

SearchHandler = React.createClass
	getInitialState: () ->
		{
			profilelist: []
		}
	render: () ->
		<div className="row">
				<div className="col s12">
					<ul className="collection with-header">
						<li className="collection-header">
							<h5 className="red-text lighten-2">Search for Users</h5>
						</li>
						<li className="collection-item">
							<input type="text" ref="query" onChange={this.handleSearch}/>
						</li>
						{this.resultNodes()}
					</ul>
				</div>
			</div>
	handleSearch: () ->
		that = this
		query = $(React.findDOMNode(this.refs.query)).val()
		if query.length > 2
			$.ajax {
				type: 'GET'
				dataType: 'json'
				url: '/api/searchprofiles/'
				data: {
					query: query
				}
				success: (profilelist) ->
					that.setState {
						profilelist: profilelist
					}
				error: (xhr, status, err) ->
					console.log status, err.toString()
			}
		else
			this.setState { profilelist: []}
	resultNodes: () ->
		this.state.profilelist.map (profile) ->
			<li className="collection-item">
				<h3><a href={"/profile/" + profile.id}>{profile.name}</a></h3>
				<p>{profile.work} &bull; {profile.location}</p>
			</li>

module.exports = SearchHandler