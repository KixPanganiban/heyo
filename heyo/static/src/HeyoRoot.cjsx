React = require "react"
Router = require "react-router"
$ = require "jquery"

RouteHandler = Router.RouteHandler

GetCurrentUserMixin = require './mixins/GetCurrentUserMixin.js'

HeyoRoot = React.createClass
	mixins: [GetCurrentUserMixin]
	getInitialState: () ->
		{
			profileId: false
		}
	componentDidMount: () ->
		this.getCurrentUser()
	render: () ->
		<div>
			<nav className="maroon darken-1" role="navigation">
				<div className="nav-wrapper container">
					<a id="logo-container" href="/" className="brand-logo">Heyo!</a>
					<ul className="right hide-on-med-and-down">
						{this.getUserLinks()}
					</ul>
					<ul id="nav-mobile" className="side-nav">
						{this.getUserLinks()}
					</ul>
					<a href="#" data-activates="nav-mobile" className="button-collapse"><i className="mdi-navigation-menu"></i></a>
				</div>
			</nav>
			<div className="section no-pad-bot" id="index-banner">
				<div className="container">
					<RouteHandler/>
				</div>
			</div>
		</div>
	getUserLinks: () ->
		if this.state.profileId
			profileLink = "/profile/" + this.state.profileId
			console.log profileLink	
			return [<li><a href="/search">Search</a></li>
					<li><a href={profileLink}>Profile</a></li>
					<li><a href="/logout/">Log out</a></li>]
		else
			return [<li><a href="/search">Search</a></li>
					<li><a href="/">Home</a></li>
					<li><a href="/login/">Login</a></li>] 

module.exports = HeyoRoot