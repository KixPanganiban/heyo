"""heyo URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.8/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.conf import settings
from django.conf.urls import include, url, patterns
from django.contrib import admin
from heyo import views

urlpatterns = patterns('',
                       url(r'^admin/', include(admin.site.urls)),
                       # Pass to react-router
                       url(r'^/?$', views.root, name='root'),
                       url(r'^logout/$', views.logoutHandler, name='logout'),
                       url(r'^login/', views.root),
                       url(r'^profile/', views.root),
                       url(r'^join/$', views.root),
                       url(r'^search/$', views.root),
                       # API routes
                       url(r'^api/join/$', views.api_joinHandler,
                           name='api_joinHandler'),
                       url(r'^api/login/$', views.api_loginHandler,
                           name='api_loginHandler'),
                       url(r'^api/facebooklogin/$', views.api_facebookLogin,
                           name='api_facebookLogin'),
                       url(r'^api/profile/', views.api_getProfile,
                           name='api_getProfile'),
                       url(r'^api/getcurrentuser/$', views.api_getCurrentUser,
                           name='api_getCurrentUser'),
                       url(r'^api/getprofiletestimonials/',
                           views.api_getProfileTestimonials,
                           name='api_getProfileTestimonials'),
                       url(r'^api/posttestimonial/', views.api_postTestimonial,
                           name='api_postTestimonial'),
                       url(r'^api/getprofilephotos/',
                           views.api_getProfilePhotos,
                           name='api_getProfilePhotos'),
                       url(r'^api/uploadphoto/', views.api_uploadPhoto,
                           name='api_uploadPhoto'),
                       url(r'^api/setprofilephoto/', views.api_setProfilePhoto,
                           name='api_setProfilePhoto'),
                       url(r'^api/getprofilephoto/', views.api_getProfilePhoto,
                           name='api_getProfilePhoto'),
                       url(r'^api/searchprofiles/', views.api_searchProfiles,
                           name='api_searchProfiles')
                       )


if settings.DEBUG:
    urlpatterns += patterns('',
                            url(r'^media/(?P<path>.*)$',
                                'django.views.static.serve', {
                                    'document_root': settings.MEDIA_ROOT,
                                }),
                            url(r'^static/(?P<path>.*)$',
                                'django.views.static.serve', {
                                    'document_root': settings.STATIC_ROOT,
                                }),
                            )
