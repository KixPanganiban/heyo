from django.contrib.auth.backends import ModelBackend
from django.contrib.auth.models import User

from heyo.models import FacebookAuth


class FacebookAuthBackend(ModelBackend):

    """
    Login to Heyo using a Facebook id
    """

    def authenticate(self, fb_user_id=None, username=None, password=None):
        """
        Returns the User attached to that fb_user_id if fb_user_id is passed,
        otherwise, do the usual auth.
        """
        if fb_user_id:
            try:
                fbauth = FacebookAuth.objects.get(
                    fb_user_id=fb_user_id)
                return fbauth.user
            except FacebookAuth.NotFound:
                return None
            except Exception, e:
                print e
                return None
        elif username and password:
            try:
                user = User.objects.get(
                    username=username)
                if user.check_password(password):
                    return user
            except User.NotFound:
                return None
            except Exception, e:
                print e
                return None
