from django.db import models
from django.contrib.auth.models import User
from django.core.exceptions import ValidationError


class Profile(models.Model):

    """
    Model for storing user Profile details.
    """
    user = models.ForeignKey(User)
    name = models.CharField(max_length=128)
    location = models.CharField(max_length=128)
    bio = models.TextField()
    work = models.CharField(max_length=128)

    def __unicode__(self):
        """
        Returns this model's Unicode string representation.
        """
        return "%s (%s)" % (self.name, self.location)

    @classmethod
    def create(cls, data):
        """
        Creates a User instance, and then creates a new Profile instance
        with the User attached via the user fkey. Accepts a dict data
        as parameter which should contain email, password, name, location,
        bio, and work. email and password are passed to User during creation.
        """
        try:
            user = User.objects.create_user(
                data["email"],
                data["email"],
                data["password"])
            profile = cls(
                user=user,
                name=data["name"],
                location=data["location"],
                bio=data["bio"],
                work=data["work"]
            )
            profile.save()
            return profile
        except Exception, e:
            if "UNIQUE constraint failed" in str(e):
                return "email_taken"
            return False

    def getPhotos(self):
        """
        Returns a list containing dictionaries of the Profile's Photos.
        """
        photos_list = []
        for photo in Photo.objects.filter(owner=self):
            photos_list.append({
                "id": photo.id,
                "url": photo.file.url
            })
        return photos_list

    def getDict(self):
        """
        Returns self as a dictionary.
        """
        try:
            profile_photo = Photo.objects.get(owner=self,
                                              is_profilephoto=True).file.url
        except:
            profile_photo = "/static/image/placeholder_avatar.png"
        selfDict = {
            "id": self.id,
            "name": self.name,
            "location": self.location,
            "bio": self.bio,
            "work": self.work,
            "profile_photo": profile_photo
        }
        return selfDict


class FacebookAuth(models.Model):
    """
    Model for storing Facebook details for a User, used to log the User in
    as well as allow the User to sign up using Facebook.
    """
    fb_user_id = models.CharField(max_length=100)
    user = models.ForeignKey(User)

    def __unicode__(self):
        """
        Returns this model's Unicode string representation.
        """
        return "%s %s" % (self.user_id, self.user.username)


class Testimonial(models.Model):

    """
    Model for storing Testimonials, which may also have children nodes
    or replies (self-referential).
    """
    target_profile = models.ForeignKey(Profile, related_name="target_profile",
                                       null=True, blank=True)
    author = models.ForeignKey(Profile, related_name="author")
    body = models.TextField()
    datetime = models.DateTimeField(auto_now_add=True)
    parent = models.ForeignKey('self', null=True, blank=True)

    def __unicode__(self):
        """
        Returns this model's Unicode string representation.
        """
        return "%s: %s" % (self.author.name, self.body)

    @classmethod
    def create(cls, data):
        """
        Creates a Testimonial based on passed data dict parameter, which
        should contain the id of the author, the body of the testimonial,
        and the parent.
        """
        try:
            if (("target_profile" not in data and "parent" not in data) or
                    ("target_profile" in data and "parent" in data)):
                raise ValidationError(
                    "Must contain target_profile XOR parent.")

            if "target_profile" in data:
                target_profile = Profile.objects.get(id=data["target_profile"])
            else:
                target_profile = None
            if "parent" in data:
                parent = Testimonial.objects.get(id=data["parent"])
            else:
                parent = None
            testimonial = cls(
                target_profile=target_profile,
                author=Profile.objects.get(id=data["author_id"]),
                body=data["body"],
                parent=parent,
            )
            testimonial.save()
            return testimonial
        except Exception, e:
            print e
            return False

    def getChildren(self):
        """
        Returns all children nodes (replies) to the current Testimonial.
        """
        return self.testimonial_set.all()

    def getDict(self):
        """
        Returns self as a dictionary.
        """
        replies = []
        for reply in self.getChildren():
            replies.append({
                "id": self.id,
                "author": {
                    "name": reply.author.name,
                    "id": reply.author.id
                },
                "body": reply.body
            })
        selfDict = {
            "id": self.id,
            "author": {
                "name": self.author.name,
                "id": self.author.id
            },
            "body": self.body,
            "replies": replies
        }
        return selfDict


class Photo(models.Model):

    """
    Model for referencing Photos and their respective files, which are attached
    to a Profile. A single Photo object may be marked as profile photo.
    If a Photo is saved with is_profilephoto as True, then all other Photos
    linked to that Profile will be marked with False.
    """
    owner = models.ForeignKey(Profile)
    file = models.ImageField(upload_to="photos/")
    datetime = models.DateTimeField(auto_now_add=True)
    is_profilephoto = models.BooleanField(default=False)

    def __unicode__(self):
        """
        Returns this model's Unicode string representation.
        """
        return "%s %s" % (self.datetime, self.owner.name)

    def save(self, *args, **kwargs):
        """
        Before calling the model's inherited .save() function, check first
        if this instance is being saved with is_profilephoto = True, and
        set all others as False.
        """
        if self.is_profilephoto is True:
            if not self.pk:
                all_photos = Photo.objects.filter(owner=self.owner)
            else:
                all_photos = Photo.objects.filter(
                    owner=self.owner).exclude(id=self.id)
            for photo in all_photos:
                photo.is_profilephoto = False
                photo.save()

        super(Photo, self).save(*args, **kwargs)

    def setProfilePhoto(self):
        """
        Sets this Photo as a profile photo by marking is_profilephoto = True
        and marking all other photos False.
        """
        for photo in Photo.objects.filter(
                owner=self.owner).exclude(id=self.id):
            photo.is_profilephoto = False
            photo.save()
        self.is_profilephoto = True
        self.save()
