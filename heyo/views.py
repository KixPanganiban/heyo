import json
from types import StringType

from django.contrib.auth import authenticate, login, logout
from django.http import HttpResponse
from django.shortcuts import render, redirect
from django.views.decorators.http import require_POST

from .models import Profile, Testimonial, Photo, FacebookAuth


def root(request):
    """
    Serve the root html template, which contains the React root and router.
    """
    return render(request, 'root.html')


def logoutHandler(request):
    """
    Logs out the current user.
    """
    logout(request)
    return redirect('root')


@require_POST
def api_joinHandler(request):
    """
    Handles form submission from join page.
    """
    data = request.POST.dict()
    print data
    # Create User
    profile = Profile.create(data)
    if isinstance(profile, Profile):
        # If successful, authenticate user and return user's profile id
        # for redirect to profile page
        try:
            user = authenticate(
                username=data["email"],
                password=data["password"])
            if user is not None:
                login(request, user)
                if "fbuserid" in data:
                    try:
                        fb = FacebookAuth(
                            user=user,
                            fb_user_id=data["fbuserid"])
                        fb.save()
                    except:
                        pass
                return HttpResponse(profile.id)
        except:
            return HttpResponse('Authentication failed.',
                                status=403,
                                reason='auth_fail')
    elif isinstance(profile, StringType):
        # If Profle.create() returns an exception, throw that exception
        # as the reason of a Bad Request and handle client-side
        return HttpResponse('Profile creation failed.',
                            status=400,
                            reason=profile)


def api_loginHandler(request):
    """
    Handles login request via form submission for login page.
    """
    data = request.POST.dict()
    # Attempt tp authenticate user and return user's profile id
    # for redirect to profile page
    try:
        user = authenticate(
            username=data["email"],
            password=data["password"])
        if user is not None:
            login(request, user)
            return HttpResponse(user.profile_set.get().id)
    except:
        return HttpResponse('Login failed.',
                            status=403,
                            reason='auth_fail')

def api_facebookLogin(request):
    """
    Handles login through Facebook.
    """
    data = request.POST.dict()
    # Attempt tp authenticate user and return user's profile id
    # for redirect to profile page
    try:
        user = authenticate(
            fb_user_id=data["id"],
            )
        if user is not None:
            login(request, user)
            return HttpResponse(user.profile_set.get().id)
    except:
        return HttpResponse('Facebook user not registered.',
                            status=403,
                            reason='no_fb_user')

def api_getProfile(request):
    """
    Retrieves a profile from a given id.
    """
    try:
        profile = Profile.objects.get(id=request.GET.get("id"))
        return HttpResponse(json.dumps(profile.getDict()))
    except Profile.DoesNotExist:
        return HttpResponse('Profile not found.',
                            status=404,
                            reason='Profile not found.')


def api_getCurrentUser(request):
    """
    Returns the logged in user's profile ID, otherwise, returns a Forbidden
    status.
    """
    try:
        profile = Profile.objects.get(id=request.user.profile_set.get().id)
        return HttpResponse(profile.id)
    except:
        return HttpResponse('Not logged in.',
                            status=403,
                            reason='User not logged in.')


def api_getProfileTestimonials(request):
    """
    Returns the JSON-serialized Testimonials for a requested user.
    """
    data = request.GET.dict()
    try:
        profile = Profile.objects.get(id=data["id"])
        testimonials = Testimonial.objects.filter(target_profile=profile)
        testimonial_list = []
        for testimonial in testimonials:
            testimonial_list.append(testimonial.getDict())
        return HttpResponse(json.dumps(testimonial_list))
    except Exception, e:
        return HttpResponse(e,
                            status=403,
                            reason=e)


def api_postTestimonial(request):
    """
    Creates a Testimonial based on the JSON object received.
    """
    data = request.POST.copy()
    try:
        if isinstance(Testimonial.create(data), Testimonial):
            profile = Profile.objects.get(id=data["view_profile"])
            testimonials = Testimonial.objects.filter(target_profile=profile)
            testimonial_list = []
            for testimonial in testimonials:
                testimonial_list.append(testimonial.getDict())
            return HttpResponse(json.dumps(testimonial_list))
    except Exception, e:
        return HttpResponse(e,
                            status=403,
                            reason=e)


def api_getProfilePhotos(request):
    """
    Returns the JSON-serialized list of a user's photos.
    """
    data = request.GET.copy()
    try:
        profile = Profile.objects.get(id=data["id"])
        return HttpResponse(json.dumps(profile.getPhotos()))
    except Profile.DoesNotExist:
        return HttpResponse('Profile not found.',
                            status=404,
                            reason='Profile not found.')


def api_uploadPhoto(request):
    """
    Handles uploading of photo.
    """
    if 'file' not in request.FILES:
        return redirect('root')
    photo = Photo(
        owner=request.user.profile_set.get(),
        file=request.FILES.get('file'),
    )
    photo.save()
    return redirect('root')


def api_setProfilePhoto(request):
    """
    Receives an id for a Photo and sets that as the user's profile photo
    if it is owned by the user.
    """
    data = request.POST.copy()
    print data
    try:
        photo = Photo.objects.get(id=data["id"])
        if photo.owner == request.user.profile_set.get():
            photo.setProfilePhoto()
            return HttpResponse(json.dumps({"status": "ok"}))
        else:
            return HttpResponse('Photo not yours.',
                                status=403,
                                reason='Photo not yours.')
    except Photo.DoesNotExist:
        return HttpResponse('Photo not found',
                            status=404,
                            reason='Photo not found.')
    except Exception, e:
        print e
        return HttpResponse(e,
                            status=403,
                            reason=e)


def api_getProfilePhoto(request):
    """
    Receives an id for which this will return the URL to the profile photo of
    the profile matching that id.
    """
    data = request.GET.copy()
    try:
        photo = Photo.objects.get(
            owner=Profile.objects.get(id=data["id"]),
            is_profilephoto=True)
        return HttpResponse(json.dumps(
            {
                "url": photo.file.url
            })
        )
    except Photo.DoesNotExist:
        return HttpResponse('Photo not found',
                            status=404,
                            reason='Photo not found.')
    except Exception, e:
        print e
        return HttpResponse(e,
                            status=403,
                            reason=e)


def api_searchProfiles(request):
    """
    Receives a string and returns all Profiles which contains that as a name
    or as a substring of.
    """
    data = request.GET.copy()
    try:
        profile_list = []
        profiles = Profile.objects.filter(
            name__contains=data["query"])
        for profile in profiles:
            profile_list.append({
                "id": profile.id,
                "name": profile.name,
                "location": profile.location,
                "work": profile.work
                })
        return HttpResponse(json.dumps(profile_list))
    except Exception, e:
        print e
        return HttpResponse(e,
                            status=403,
                            reason=e)
