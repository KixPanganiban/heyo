# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('heyo', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Photo',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('file', models.ImageField(upload_to=b'photos/')),
                ('datetime', models.DateTimeField(auto_now_add=True)),
                ('is_profilephoto', models.BooleanField(default=False)),
                ('owner', models.ForeignKey(to='heyo.Profile')),
            ],
        ),
    ]
