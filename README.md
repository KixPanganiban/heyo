# Heyo
Heyo is a mini social network prototype using ReactJS + Django.

# Installation
You need the `python-dev` and `build-essential` packages if you're running Ubuntu (in Arch these are already installed). Install via `sudo apt-get install python-dev build-essential -y` if you're using apt as your package manager. These are required for compiling Pillow, a dependency for image processing in Django. Without those, you are likely to face missing file errors during installation.

Install the Python packages with `pip install -r requirements.txt`.

Run `fab init_django` to initialize the SQLite database (it will ask you for a superuser account), as well as collect the static files (in case you intend to use it behind Gunicorn).

# Running
The Heyo Facebook app used to sign up and log in with Facebook is pointed to `http://localhost:8000` as its web page. Please run your Django server there to avoid permission conflicts with Facebook.

## Running locally
As you would a normal Django project, run `python manage.py runserver`.

## Running on production
**Setup nginx:**
Use Fabric to setup nginx and run/stop/restart the Gunicorn daemon. To setup nginx, run `fab init_nginx`. This assumes that you do not currently have a working nginx installation: it installs one for you, and then overwrites the `nginx.conf` with the one bundled with Heyo. If you already have a working nginx installation, copy the the configuration in `deploy/nginx.conf` into your `/etc/nginx/nginx.conf`.
> Important: The provided `nginx.conf` assumes that Heyo is installed in `/var/app/heyo` and that user `ubuntu` in usergroup `ubuntu` has the correct read/write/execute privileges. Otherwise, make modifications to lines `3` and `30` to `deploy/nginx.conf` before running `fab init_nginx`.

**Run the gunicorn daemon:**
Run `fab run_gunicorn` to start the gunicorn daemon.

**Stop/restart the gunicorn daemon:**
Run `fab stop_gunicorn` to stop the daemon, or `restart_gunicorn` to restart it.

**Running in production mode:**
Deleting `heyo/local.py` sets debug=False, useful when running in production.
### CJSX Files
The front-end is built with ReactJS, using the CJSX Grunt compiler. To modify the CJSX files and compile new build files, please install Grunt and its dependencies by running `npm install` at the root directory. This, obviously, requires a working NodeJS and npm installation.
> Note: To conserve zip file size, the node_modules directory which contains Grunt, Bower, and dependencies has not been included. Please install by running `npm install`.

### Bower Files
External assets are managed by Bower. Bower is also installed via `npm install`, and all components may be re-downloaded by running `bower install`.

# Questions?
Please feel free to contact me at kixpanganiban [at] gmail [dot] com. Thank you!