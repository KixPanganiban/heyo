"""
fabfile for Heyo
"""

from fabric.api import *
from fabric.tasks import execute


def init_nginx():
    """
    Installs nginx, and overwrites the current nginx.conf file with the one
    provided by Heyo.

    Don't use if you have an existing nginx setup! Instead, we recommend
    you to copy the necessary server configs in deploy/nginx.conf instead.
    """

    local('sudo apt-get install nginx -y')

    local('sudo mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf.bak')
    local('sudo cp ./deploy/nginx.conf /etc/nginx/nginx.conf')

    local('sudo killall -9 nginx')
    local('sudo service nginx start')


def init_django():
    """
    Creates the sqlite database and collects the static files.
    """

    local('python manage.py syncdb')
    local('python manage.py collectstatic')


def start_gunicorn():
    """
    Runs a gunicorn daemon.
    """
    local('gunicorn heyo.wsgi -D')


def stop_gunicorn():
    """
    Stops the gunicorn daemon.
    """

    # WARNING: This is "dirty" code that kills all instances of gunicorn.
    # As such, if more than one gunicorn daemon is running the system,
    # perhaps consider using Gaffer or Supervisor.
    local('sudo killall -9 gunicorn')


def restart_gunicorn():
    """
    Restarts the guncicorn daemon.
    """
    execute('stop_gunicorn')
    execute('start_gunicorn')
